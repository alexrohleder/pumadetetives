<?php

return array(
	'header' => 'Localização de Pessoas',
    'description' => 'Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.',
	'content' => '<p class="text-justify">O trabalho de localização consiste em um serviço de indiligência com o objetivo de localizar familiares, golpistas ou qualquer outra pessoa desaparecida da qual se tenha as informações prévias para dar andamento as investigações</p>
<p class="text-justify">O tempo necessário e o custo para localizar uma pessoa vão depender das caracteristicas do caso, o tempo do último contato e o perfil da pessoa procurada. Em alguns casos a localização é realizada em apenas um dia após a contratação, em outros o contratante deve ter ciência da probabilidade da pessoa er encontrada em óbito.</p>
<p class="text-justify">Localizamos devedores, estelionatários, testemunhas, filhos extra conjugais. O trabalho de localização também depende de alguns fatores como: A pessoa quer ser achada? Está fugindo? Quanto tempo você não vê essa pessoa? Possui o nome completo? Possui CPF?</p>'
);
