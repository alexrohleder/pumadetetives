<?php

return array(
	'header' => 'Investigação Juridica',
    'description' => 'Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.',
	'content' => '<p  class="text-justify">A vitória nos tribunais depende fundamentalmente da apresentação de provas confiáveis baseadas em fatos irrefutáveis posto que verdadeiros. Êxitos obtidos em muitas causas tiveram por suporte investigações confiáveis e oportunas, tais como, relatórios produzidos no termino d eum trabalho, fotos anexadas a estes, localização de réus e testemunhas, levantamento e identificação de patrimônio.</p>
<p  class="text-justify">Ocorrências como furtos, fraudes, desvios, apropriações indevidas, extorsões e outros atos criminosos também podem ter solução, desde que, investigados por profissionais de comprovada experiência. A Puma Detetives tem como propósito básico colaborar, através do suporte sigiloso e legal, com escritórios de advocacia e seus clientes na solução de seus casos.</p>'
);
