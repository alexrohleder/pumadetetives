<?php

return array(
	'header' => 'Investigação Especifica',
    'description' => 'Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.',
	'content' => '<p class="text-justify">Investigar ou não investigar? Realmente, eis a questão. Descobrir ou não descobrir? Muitos tem a dúvida e a querem sanar, outros, tem a certeza e a querem comprovar. Se em algum momento da vida nós temos algo que nos tira o sossego e até mesmo noites de sono, a dúvida principal é: Qual a hora certa de chegar a verdade?</p>
<p class="text-justify">Afinal, o esclarecimento é algo muito simples, quando se conta com a atuação de profissionais competentes, detetives particulares especializados na área. Assim, a angústia acaba se localizando em uma decisão de foro íntimo: enfrentar ou não a verdade? Esse dilema certamente será desfeito no momento oportuno, quando você estiver preparado, quem sabe após orientações de seu advogado ou analista.</p>
<p class="text-justify">Importante é que você tenha tranquilidade de que, quando a hora oportuna chegar, e você estiver pronto para solucionar as questões que lhe geram desconforto e ansiedade, muitas vezes causando prejuizos a seu patrimônio ou a sua saúde emocional, a Puma Detetives está pronta a lhe atender, com toda a comodidade e segurança que a empresa de mais de 10 anos de atendimento no setor de detetives particulares.</p>'
);
