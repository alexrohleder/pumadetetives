<?php

return array(
	'header' => 'Investigação Empresarial',
    'description' => 'Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.',
	'content' => '<p class="text-justify">A Puma Detetives possui meios de promover a execução de ações de natureza defensiva e preventiva para aumentar a competitividade de sua empresa, nossos profissionais são especializados em investigar desfalques, fraudes, espionagens, funcionários, sócios, concorrentes, e atos ilícitos que estejam ocorrendo sem seu conhecimento.</p>
<p class="text-justify">Muitos empreendedores sofrem com os problemas citados, resultando em queda de produção ou interrompendo seu crescimento profissional e financeiro. Estes podem ser solucionados com a infiltração de agentes entre seus funcionários, acompanhamentos, varreduras ambientais, sistemas de contra espionagem, consulte-nos e resolva seus problemas de forma inteligente.</p>'
);
