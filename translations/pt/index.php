﻿<?php

return array(
	'title' => 'Puma Detetives - Investigação Profissional em Santa Maria RS',
	'carrousel_inicial' => 'Cada vez mais vivemos numa sociedade onde é crucial dominar a informação que nos rodeia, e a investigação privada é a forma de alcançar esse objetivo!',
	'veja_nossos_servicos' => 'Veja Nossos Serviços',
	'saiba_mais' => 'Saiba Mais',
	'contate_nos' => 'Contate-nos',
	'monitoramento_virtual' => 'Monitoramento Virtual',
	'monitoramento_virtual_descricao' => 'Descubra o conteúdo de mensagens do facebook, whatsapp, e-mail, sms e ligações telefonicas.',
	'dossies_fisicos_juridicos' => 'Dossiês Físicos e Jurídicos',
	'dossies_fisicos_juridicos_descricao' => 'Levantamento de dados sigilosos de uma empresa ou pessoa, e informações que comprovam um fato.',
	'investigacao_empresarial' => 'Investigação Empresarial',
	'investigacao_empresarial_descricao' => 'Oferecemos Serviços de natureza defensiva e preventiva, contra-espionagem, investigação de sócios, funcionários, concorrentes e furtos de mercadorias.',
	'investigacao_juridica' => 'Investigação Jurídica',
	'investigacao_juridica_descricao' => 'Coleta de provas sólidas e inconstestáveis de todas as áreas do direito com validade judicial.',
	'infidelidade_conjugal' => 'Infidelidade Conjugal',
	'infidelidade_conjugal_descricao' => 'Somos peritos em flagrantes de adultério, contrate nossos serviços e não seja o último a saber.',
	'trabalhos_investigativos_com' => 'Trabalhos investigativos com',
	'etica' => 'Ética',
	'sigilo' => 'Sigilo',
	'tecnologia' => 'tecnologia',
	'discricao' => 'discrição',
	'agencia_descricao_p1' => 'Atua em todo território nacional e encontra-se capacitada para atender seus clientes em todos os segmentos da área de Inteligência, sempre em busca de um elevado padrão de qualidade a Puma Detetives orgulha-se de ter resolvido inúmeros casos que atingiram o objetivo e a satisfação dos clientes, aliando sua técnica, eficiência e perseverança com equipamentos modernos e eficazes.',
	'agencia_descricao_p2' => 'Todo seu trabalho é firmado através de contrato detalhado, lavrado em 02 vias de igual teor e forma, possibilitando assim, a você cliente, tomar uma decisão imediata e contratar um dos melhores serviços reservados de inteligência que há no mercado Nacional.',
	'agencia_descricao_confianca' => '<b>Confiança</b>, integridade, crença e sobretudo respeito mútuo são valores indispensáveis que devem nortear o relacionamento cliente/empresa. A empresa entende que seus clientes precisam estar informados, com precisão e presteza de tudo que se relacionar com o fato em análise.',
	'agencia_descricao_exito' => 'O conceito de <b>êxito</b> não pode ser avaliado por aquilo que o cliente espera ao lhe ser entregue o relatório final do trabalho. Esta colocação torna-se oportuna uma vez que pode ocorrer que o lhe está sendo relatado possa diferir daquilo que ele esperava encontrar.',
	'agencia_descricao_sigilo' => 'O cliente deve estar ciente de que todos os fatos ligados ao trabalho em execução estarão protegidos pelo mais absoluto <b>sigilo</b>. O sigilo por parte do cliente é fundamental, a menor indiscrição sobre o curso do trabalho, pode por em risco não só o serviço como a segurança dos agentes.'	
);
