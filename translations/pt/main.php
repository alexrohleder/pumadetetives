<?php

return array(
	'lang' => 'pt',
	'browsehappy' => 'Você está usando um <strong>navegador desatualizado</strong>. Por favor <a href="http://browsehappy.com/">atualize seu navegador</a> para melhorar sua experiência.',
	'noscript' => 'Infelizmente seu navegador <strong>desativou a execução de scripts</strong>. Por favor ative esse recurso para que esta página funcione corretamente.',
	'nossa_lista_servicos' => 'Nossa lista de',
	'nossa_lista_servicos_text' => 'Oferecemos uma ampla lista de serviços para várias áreas da investigação. É importante salientar que somos extremamente defensores da moral e bons costumes, em hipótese alguma aceitamos trabalhos de natureza ilícita. Tendo isto em mente conte com nossa experiente equipe de detetives particulares. Palavras da diretoria.',
	'investigacao' => 'Investigação',
	'conjugal' => 'Conjugal',
	'empresarial' => 'Empresarial',
	'politica' => 'Politica',
	'especifica' => 'Especifica',
	'localizacao' => 'Localização',
	'pessoas' => 'Pessoas',
	'bens' => 'Bens',
	'virtual' => 'Virtual',
	'monitoramento' => 'Monitoramento',
	'pessoal' => 'Pessoal',
	'servicos_a1' => 'Provas para separação judicial, divórcio e guarda de filhos',
	'servicos_a2' => 'Flagrantes',
	'servicos_a3' => 'Localização e busca de animais roubados',
	'servicos_a4' => 'Investigação pré-matrimonial',
	'servicos_legal' => 'Todos os nossos serviços estão regulamentados com base na legislação brasileira atual, nossos colaboradores estão altamente preparados para realizar desde uma consultoria a um serviço extremo de localização em países vizinhos.',
	'cobertura' => 'Cobertura',
	'de_servicos' => 'de Serviços',
	'cobertura_de_servicos' => 'Realizamos todos os serviços especificos de investigação e localização sob amplitude legal, localizações e recuperações de veiculos automotores desaparecidos ou furtados dentro do território brasileiro serão cobrados proporcionalmente a 20% do valor do veículo, sendo pago 24 horas antes da retirada. Não nos responsabilizamos por taxas de transporte e avarias(em caso de avarias, será cobrado o proporcional, sendo tabela menos valor conserto). Empresas, financeiras, bancos e companhias de seguro tabeladas 10% sobre avaliação do veículo.',
	'contate' => 'Contate',
	'nossos_detetives' => 'nossos Detetives',
	'contate_nossos_detetives_p1' => 'Atendimento 24 horas durante todos os dias da semana. Responderemos mensagens do facebook, google plus ou e-mail dentro de dois dias.',
	'contate_nossos_detetives_p2' => 'Sinta-se a vontade em realizar contato com um de nossos detetives, o sigilo é absoluto!',
	'servicos' => 'Serviços'
);
