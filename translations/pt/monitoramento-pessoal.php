<?php

return array(
	'header' => 'Monitoramento Pessoal',
    'description' => 'Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.',
	'content' => '<p class="text-justify">Oferecemos o serviço de monitoramento feito através de rastreador pessoal veicular com acompanhamento feito em tempo real, 24 horas por dia. O monitoramento feito através de GPS ajuda a localizar não só o veiculo, mas também delimita uma área geográfica e identifica quando a pessoa passou pelo local e permite também saber exatamente onde a pessoa está através de identificação de seu veículo.</p>'
);
