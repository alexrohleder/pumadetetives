<?php

return array(
	'header' => 'Localização de Animais',
    'description' => 'Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.',
	'content' => '<p class="text-justify">O indiica de animais desaparecidos é muito grande em todo mundo, no Brasil são registrados centenas de boletins de ocorrência sobre o desaparecimento de animais de diversas espécies diariamente. O desaparecimento de bichos de estimação como gatos, cachorros e papagaios correspondem a maior parte das ocorrências. Na maioria das vezes ocorrem por descuido de seus proprietários.</p>
<p class="text-justify">Por outro lado são comuns os furtos de cavalos e bois, principalmente nas regiões do Rio Grande do Sul, Goiás, Mato Grosso e Tocantins. Quadrilhas altamente especializadas em roubo e furto de animais agem de forma bem planejada dificultando o trabalho das autoridades locais que muitas vezes não possuem meios e recursos tecnológicos adequados para trabalhas.</p>
<p class="text-justify">A agência Puma Detetives vem realizando serviços de localização e busca de animais, denunciando os infratores e reavendo os prejuizos dos proprietários. Dispomos de técnicas, pessoal e equipamentos especiais para este fim.</p>'
);
