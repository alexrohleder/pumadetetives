<?php

return array(
	'header' => 'Localização de Bens',
    'description' => 'Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.',
	'content' => '<p class="text-justify">Com confirmação através de certidão de cartórios, certidão de registros de imóveis, ou cadastro com os respectivos registros, folhas e livros. A Puma Detetives está capacitada a encontrar tanto bens móveis quanto imóveis com pesquisas abrangentes realizadas em bancos, cartórios, registros de imóveis, detrans, ciretrans, ministério da marinha, ministério da aeronáutica, ministério da fazenda, e outros meios de informação.</p>'
);
