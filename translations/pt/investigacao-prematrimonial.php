<?php

return array(
	'header' => 'Investigação Pré Matrimonial',
    'description' => 'Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.',
	'content' => '<p class="text-justify">Amplamente difundida na Europa e nos Estados Unidos, agora esta absoluta novidade esta sendo implantada com sucesso no Brasil. A investigação pré-matrimonial tem por objetivo principal a coleta de provas e informações seguras para um futuro matrimonio ou convivências felizes e tranquilas.</p>'
);
