<?php

return array(
	'header' => 'Investigação Conjugal',
    'description' => 'Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.',
	'content' => '<p class="text-justify">A infidelidade conjugal é sem dúvida, a parte classica das investigações particulares, portanto, sendo o principal serviço dos nossos detetives profissionais, a anos é oferecido pela Puma Detetives com enorme sucesso. Trata-se de um dos trabalhos mais delicados porque tem como objetivo coletar provas sobre pessoas que fazem parte do próprio convivio e circulo sentimental.</p>
<p class="text-justify">Sem descurar do cumprimento do ordenamento jurídico, dispomos de equipamentos de última geração e, sobre tudo, equipes de detetives profissionais, masculinas e femininas, altamente treinados e discretos, que, sem nunca menosprezar o caráter e a inteligência do conjugue/convivente, buscam coletar provas e informações que formarão uma tendência cientifica e indicar uma convicção por meio de um relatório final capaz de esclarecer qualquer tipo de dúvida.</p>'
);
