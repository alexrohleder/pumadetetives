<?php

/* service.twig */
class __TwigTemplate_13ae8d109006e6cca32853aa70c641407c062fc8f4d2a33dbf98e3958647ee36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("template.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "
    <title>";
        // line 5
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "header", array(), "array"), "html", null, true);
        echo " - Puma Detetives</title>
    <meta name=\"description\" content=\"";
        // line 6
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "description", array()), "html", null, true);
        echo "\">

    <meta property=\"og:title\" content=\"";
        // line 8
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "header", array(), "array"), "html", null, true);
        echo "\">
    <meta property=\"og:url\" content=\"http://www.pumadetetives.com.br";
        // line 9
        if (isset($context["uri"])) { $_uri_ = $context["uri"]; } else { $_uri_ = null; }
        echo twig_escape_filter($this->env, $_uri_, "html", null, true);
        echo "\">
    <meta property=\"og:description\" content=\"";
        // line 10
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "description", array()), "html", null, true);
        echo "\">

";
    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        // line 15
        echo "
\t<div class=\"row content-header-row\" id=\"services\">
        <div class=\"col-xs-12\">
            <h1 class=\"text-center text-capitalize\">
                ";
        // line 19
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "header", array(), "array"), "html", null, true);
        echo "
            </h1>
        </div>
    </div>
    <div class=\"row section-row\">
        <div class=\"col-xs-12\">
            <div class=\"container\">
                <div class=\"col-xs-12\">
                \t";
        // line 27
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo $this->getAttribute($_lang_, "content", array(), "array");
        echo "
                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "service.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 27,  81 => 19,  75 => 15,  72 => 14,  64 => 10,  59 => 9,  54 => 8,  48 => 6,  43 => 5,  40 => 4,  37 => 3,  11 => 1,);
    }
}
