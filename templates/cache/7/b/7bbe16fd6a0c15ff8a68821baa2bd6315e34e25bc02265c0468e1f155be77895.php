<?php

/* service.twig */
class __TwigTemplate_7bbe16fd6a0c15ff8a68821baa2bd6315e34e25bc02265c0468e1f155be77895 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("template.twig", "service.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "
    <title>";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "header", array(), "array"), "html", null, true);
        echo " - Puma Detetives</title>
    <meta name=\"description\" content=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "description", array()), "html", null, true);
        echo "\">

    <meta property=\"og:title\" content=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "header", array(), "array"), "html", null, true);
        echo "\">
    <meta property=\"og:url\" content=\"http://www.pumadetetives.com.br";
        // line 9
        echo twig_escape_filter($this->env, (isset($context["uri"]) ? $context["uri"] : null), "html", null, true);
        echo "\">
    <meta property=\"og:description\" content=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "description", array()), "html", null, true);
        echo "\">

";
    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        // line 15
        echo "
\t<div class=\"row content-header-row\" id=\"services\">
        <div class=\"col-xs-12\">
            <h1 class=\"text-center text-capitalize\">
                ";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "header", array(), "array"), "html", null, true);
        echo "
            </h1>
        </div>
    </div>
    <div class=\"row section-row\">
        <div class=\"col-xs-12\">
            <div class=\"container\">
                <div class=\"col-xs-12\">
                \t";
        // line 27
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "content", array(), "array");
        echo "
                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "service.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 27,  68 => 19,  62 => 15,  59 => 14,  52 => 10,  48 => 9,  44 => 8,  39 => 6,  35 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
