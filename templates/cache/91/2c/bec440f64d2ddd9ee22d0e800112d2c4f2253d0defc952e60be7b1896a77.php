<?php

/* index.twig */
class __TwigTemplate_912cbec440f64d2ddd9ee22d0e800112d2c4f2253d0defc952e60be7b1896a77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("template.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "
    <title>";
        // line 5
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "title", array(), "array"), "html", null, true);
        echo "</title>
    <meta name=\"description\" content=\"Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.\">

    <meta property=\"og:title\" content=\"Puma Detetives\">
    <meta property=\"og:url\" content=\"http://www.pumadetetives.com.br\">
    <meta property=\"og:description\" content=\"Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.\">

";
    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        // line 15
        echo "
    <div class=\"row carrousel\">

        <span class=\"progress\"></span>

        <div class=\"col-xs-12 current\" style=\"background-image:url(img/carrousel/dossier.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1 class=\"text-center text-adaptative\">";
        // line 26
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "carrousel_inicial", array(), "array"), "html", null, true);
        echo "</h1>

                        <center>
                            <a href=\"#services\" class=\"btn btn-inverse\">";
        // line 29
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "veja_nossos_servicos", array(), "array"), "html", null, true);
        echo "</a>
                            <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 30
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "contate_nos", array(), "array"), "html", null, true);
        echo "</a>
                        </center>

                    </div>
                </div>
            </div>

        </div>

        <div class=\"col-xs-12\" style=\"background-image:url(img/carrousel/virtual.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1>";
        // line 45
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "monitoramento_virtual", array(), "array"), "html", null, true);
        echo "</h1>
                        <h3>";
        // line 46
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "monitoramento_virtual_descricao", array(), "array"), "html", null, true);
        echo "</h3>
                        <a href=\"/";
        // line 47
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "lang", array(), "array"), "html", null, true);
        echo "/monitoramento-virtual\" class=\"btn btn-inverse\">";
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "saiba_mais", array(), "array"), "html", null, true);
        echo "</a>
                        <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 48
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "contate_nos", array(), "array"), "html", null, true);
        echo "</a>

                    </div>
                </div>
            </div>

        </div>

        <div class=\"col-xs-12\" style=\"background-image:url(img/carrousel/dossier.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1>";
        // line 62
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "dossies_fisicos_juridicos", array(), "array"), "html", null, true);
        echo "</h1>
                        <h3>";
        // line 63
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "dossies_fisicos_juridicos_descricao", array(), "array"), "html", null, true);
        echo "</h3>
                        <a href=\"/";
        // line 64
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "lang", array(), "array"), "html", null, true);
        echo "/investigacao-juridica\" class=\"btn btn-inverse\">";
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "saiba_mais", array(), "array"), "html", null, true);
        echo "</a>
                        <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 65
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "contate_nos", array(), "array"), "html", null, true);
        echo "</a>

                    </div>
                </div>
            </div>

        </div>

        <div class=\"col-xs-12\" style=\"background-image:url(img/carrousel/empresarial.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1>";
        // line 79
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "investigacao_empresarial", array(), "array"), "html", null, true);
        echo "</h1>
                        <h3>";
        // line 80
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "investigacao_empresarial_descricao", array(), "array"), "html", null, true);
        echo "</h3>
                        <a href=\"/";
        // line 81
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "lang", array(), "array"), "html", null, true);
        echo "/investigacao-empresarial\" class=\"btn btn-inverse\">";
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "saiba_mais", array(), "array"), "html", null, true);
        echo "</a>
                        <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 82
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "contate_nos", array(), "array"), "html", null, true);
        echo "</a>

                    </div>
                </div>
            </div>

        </div>

        <div class=\"col-xs-12\" style=\"background-image:url(img/carrousel/juridico.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1>";
        // line 96
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "investigacao_juridica", array(), "array"), "html", null, true);
        echo "</h1>
                        <h3>";
        // line 97
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "investigacao_juridica_descricao", array(), "array"), "html", null, true);
        echo "</h3>
                        <a href=\"/";
        // line 98
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "lang", array(), "array"), "html", null, true);
        echo "/investigacao-juridica\" class=\"btn btn-inverse\">";
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "saiba_mais", array(), "array"), "html", null, true);
        echo "</a>
                        <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 99
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "contate_nos", array(), "array"), "html", null, true);
        echo "</a>

                    </div>
                </div>
            </div>

        </div>

        <div class=\"col-xs-12\" style=\"background-image:url(img/carrousel/infidelidade.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1>";
        // line 113
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "infidelidade_conjugal", array(), "array"), "html", null, true);
        echo "</h1>
                        <h3>";
        // line 114
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "infidelidade_conjugal_descricao", array(), "array"), "html", null, true);
        echo "</h3>
                        <a href=\"/";
        // line 115
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "lang", array(), "array"), "html", null, true);
        echo "/investigacao-conjugal\" class=\"btn btn-inverse\">";
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "saiba_mais", array(), "array"), "html", null, true);
        echo "</a>
                        <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 116
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "contate_nos", array(), "array"), "html", null, true);
        echo "</a>

                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class=\"row section-row\" id=\"about\">
        <div class=\"col-xs-12\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1 class=\"text-center\">
                            ";
        // line 133
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "trabalhos_investigativos_com", array(), "array"), "html", null, true);
        echo "
                            <span class=\"text-rotator\">
                                <span>";
        // line 135
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "etica", array(), "array"), "html", null, true);
        echo "</span>
                                <span>";
        // line 136
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "tecnologia", array(), "array"), "html", null, true);
        echo "</span>
                                <span>";
        // line 137
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "sigilo", array(), "array"), "html", null, true);
        echo "</span>
                                <span>";
        // line 138
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "discricao", array(), "array"), "html", null, true);
        echo "</span>
                            </span>
                        </h1>

                        <p class=\"text-justify\">";
        // line 142
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "agencia_descricao_p1", array(), "array"), "html", null, true);
        echo "</p>
                        <p class=\"text-justify\">";
        // line 143
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "agencia_descricao_p2", array(), "array"), "html", null, true);
        echo "</p>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"row section-row\">
        <div class=\"col-xs-12\">
            <div class=\"container\">
                <div class=\"row\">

                    <div class=\"col-xs-12 col-md-4\">
                        <p class=\"text-justify\">";
        // line 157
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo $this->getAttribute($_lang_, "agencia_descricao_confianca", array(), "array");
        echo "</p>
                    </div>

                    <div class=\"clearfix visible-xs-block\"></div>

                    <div class=\"col-xs-12 col-md-4\">
                        <p class=\"text-justify\">";
        // line 163
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo $this->getAttribute($_lang_, "agencia_descricao_exito", array(), "array");
        echo "</p>
                    </div>

                    <div class=\"clearfix visible-xs-block\"></div>

                    <div class=\"col-xs-12 col-md-4\">
                        <p class=\"text-justify\">";
        // line 169
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo $this->getAttribute($_lang_, "agencia_descricao_sigilo", array(), "array");
        echo "</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  358 => 169,  348 => 163,  338 => 157,  320 => 143,  315 => 142,  307 => 138,  302 => 137,  297 => 136,  292 => 135,  286 => 133,  265 => 116,  257 => 115,  252 => 114,  247 => 113,  229 => 99,  221 => 98,  216 => 97,  211 => 96,  193 => 82,  185 => 81,  180 => 80,  175 => 79,  157 => 65,  149 => 64,  144 => 63,  139 => 62,  121 => 48,  113 => 47,  108 => 46,  103 => 45,  84 => 30,  79 => 29,  72 => 26,  59 => 15,  56 => 14,  43 => 5,  40 => 4,  37 => 3,  11 => 1,);
    }
}
