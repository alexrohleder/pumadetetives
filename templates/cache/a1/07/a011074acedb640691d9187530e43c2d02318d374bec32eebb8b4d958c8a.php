<?php

/* 404.twig */
class __TwigTemplate_a107a011074acedb640691d9187530e43c2d02318d374bec32eebb8b4d958c8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("template.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "
\t<title>Puma Detetives</title>

";
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "
\t<div class=\"row section-row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<h1 class=\"text-center\">";
        // line 13
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "h1", array(), "array"), "html", null, true);
        echo "</h1>
\t\t\t<h2 class=\"text-center\">";
        // line 14
        if (isset($context["lang"])) { $_lang_ = $context["lang"]; } else { $_lang_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_lang_, "h2", array(), "array"), "html", null, true);
        echo "</h2>
\t\t</div>
\t</div>

";
    }

    public function getTemplateName()
    {
        return "404.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 14,  55 => 13,  50 => 10,  47 => 9,  40 => 4,  37 => 3,  11 => 1,);
    }
}
