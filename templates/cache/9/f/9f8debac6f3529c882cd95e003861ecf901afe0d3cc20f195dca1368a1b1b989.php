<?php

/* template.twig */
class __TwigTemplate_9f8debac6f3529c882cd95e003861ecf901afe0d3cc20f195dca1368a1b1b989 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"pt\" class=\"no-js\">

    <head>

        <base href=\"/\">
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
        <meta name=\"google-site-verification\" content=\"CJJIBLTws18J1p5A_xKjJKPbR9cZWiRV93X660kpJPA\">

        <meta property=\"og:type\" content=\"website\">
        <meta property=\"og:site_name\" content=\"Puma Detetives\">
        <meta property=\"og:image\" content=\"http://www.pumadetetives.com.br/img/facebook-thumb.jpg\">
        
        <link rel=\"shortcut icon\" href=\"http://localhost/img/favicon/32x32.png\">
        <link rel=\"publisher\" href=\"https://plus.google.com/113863419996585652167\">
        <link rel=\"alternate\" hreflang=\"x-default\" href=\"http://www.pumadetetives.com.br\">
        <link rel=\"alternate\" hreflang=\"pt-BR\" href=\"http://www.pumadetetives.com.br\">

        <script type=\"text/javascript\" src=\"http://localhost/js/vendor/modernizr.js\"></script>

        <link type=\"text/css\" rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Lato:300,400,700,900\">
        <link type=\"text/css\" rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\">

        <link type=\"text/css\" rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\">
        <link type=\"text/css\" rel=\"stylesheet\" href=\"http://localhost/css/main.css\">

        <meta name=\"author\" content=\"Puma Detetives\">
        <meta name=\"keywords\" content=\"detetive particular, santa maria, rio grande do sul, rs, santa maria rs, investigação, traição, flagrante, apreenção, localização de pessoas, provas judiciais, emissão de docies\">

        ";
        // line 32
        $this->displayBlock('head', $context, $blocks);
        // line 35
        echo "
    </head>
   <body>

        <!-- Support Markup - Caso o navegador não suporte algumas features utilizadas pelo site e/ou o JavaScript esteja desabilitado, mensagens de alerta serão exibidas. -->
        <p class=\"announce browsehappy hidden\" id=\"browsehappy\">";
        // line 40
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "browsehappy", array(), "array");
        echo "</p>
        <noscript><p class=\"annouce noscript\">";
        // line 41
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "noscript", array(), "array");
        echo "</p></noscript>
        <!-- End of Support Markup -->

        <div class=\"landing\">
            <div class=\"logo\">
                <svg width=\"100%\" height=\"100%\" viewBox=\"0 0 240 240\" preserveAspectRatio=\"xMidYMid meet\">
                <path transform=\"translate(0, 240) scale(0.1, -0.1)\"
                      d=\"M1145 1550 c-38 -25 -109 -26 -221 -4 -75 15 -86 15 -137 0 -70 -20
                      -107 -63 -107 -124 0 -72 78 -125 159 -107 17 4 31 4 31 0 0 -13 -33 -55 -44
                      -55 -18 -1 -133 -187 -122 -198 3 -2 21 -8 42 -12 64 -15 102 15 75 59 -14 21
                      -2 41 24 41 16 0 39 18 72 55 49 58 93 80 147 73 38 -4 57 -33 33 -50 -14 -11
                      -15 -19 -6 -50 5 -21 16 -38 24 -38 8 0 19 -9 25 -20 6 -11 17 -20 26 -20 8 0
                      19 -12 25 -27 5 -16 13 -35 16 -43 4 -8 7 -24 7 -35 1 -17 9 -20 64 -23 59 -3
                      62 -2 62 20 0 32 -11 45 -43 53 -33 9 -53 40 -38 59 8 10 7 17 -4 26 -8 7 -15
                      21 -15 31 0 18 2 19 30 4 35 -18 54 -19 79 -3 45 28 143 -37 161 -107 7 -26
                      70 -35 70 -11 0 9 2 16 4 16 10 0 55 -27 70 -43 23 -23 94 -23 103 -1 9 24
                      -10 48 -43 55 -16 3 -42 14 -58 24 -28 19 -28 20 -22 85 6 71 1 87 -21 69 -10
                      -9 -26 -1 -69 36 -30 26 -72 67 -92 91 -20 24 -44 44 -53 44 -9 0 -19 6 -22
                      14 -4 11 -14 12 -39 7 -20 -5 -59 -2 -98 6 -117 25 -234 2 -332 -65 -79 -55
                      -146 -51 -191 10 -20 27 -20 29 -4 62 17 36 48 58 101 70 19 5 66 -1 127 -15
                      75 -17 111 -20 163 -15 72 8 114 32 103 59 -8 22 -25 21 -62 -3z\">
                </svg>
            </div>
            <div class=\"load\">
                <svg width=\"60px\" height=\"60px\" viewBox=\"0 0 80 80\">
                    <path class=\"bg\" d=\"M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z\" />
                    <path class=\"tg\" d=\"M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z\" />
                </svg>
            </div>
        </div>

        <div class=\"container-fluid\">

            <div class=\"row contact-row\" id=\"top\">
                <div class=\"col-xs-12\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <a class=\"col-xs-6 col-sm-1\" href=\"https://www.facebook.com/pumadetetives.br\" rel=\"Facebook\" title=\"Facebook\">
                                <i class=\"fa fa-facebook\"></i>
                            </a>
                            <a class=\"col-xs-6 col-md-5\" href=\"mailto:pumadetetives@outlook.com\" rel=\"email\" title=\"Envie um email\">
                                <i class=\"fa fa-envelope\"></i> <span class=\"visible-md-inline visible-lg-inline\">pumadetetives@outlook.com</span>
                            </a>
                            <a class=\"col-xs-12 col-md-3\" href=\"tel:55999223814\" title=\"Ligar\">
                                <b><i class=\"fa fa-phone\"></i> <span>055 9 9922-3814</span></b>
                            </a>
                            <a class=\"col-xs-12 col-md-3\" href=\"tel:55996741018\" title=\"Ligar\">
                                <b><i class=\"fa fa-phone\"></i> <span>055 9 9674-1018</span></b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row\">
                <nav class=\"navbar navbar-default\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"/\" title=\"Puma Detetives\">
                                <img src=\"img/logo.png\" alt=\"Puma Detetives\"><h1>Puma Detetives</h1>
                            </a>
                        </div>
                        <div class=\"navbar-right visible-md-block visible-lg-block\">
                            <ul class=\"nav navbar-nav\">
                                <li><a href=\"/#about\" title=\"Sobre a Empresa\">Sobre</a></li>
                                <li><a href=\"#services\" title=\"Serviços Oferecidos\">Serviços</a></li>
                                <li><a href=\"#contact\" title=\"Contate um de nossos detetives\">Contato</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            ";
        // line 114
        $this->displayBlock('content', $context, $blocks);
        // line 117
        echo "
            <div class=\"row section-row\" id=\"services\">
                <div class=\"col-xs-12\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xs-12\">

                                <h1 class=\"text-right\">";
        // line 124
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "nossa_lista_servicos", array(), "array"), "html", null, true);
        echo " <span class=\"text-featured\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "servicos", array(), "array"), "html", null, true);
        echo "</span></h1>

                                <br>

                                <p class=\"text-justify\">";
        // line 128
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "nossa_lista_servicos_text", array(), "array"), "html", null, true);
        echo "</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row galery\">

                <a class=\"col-xs-12 col-md-3\" href=\"/";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/investigacao-conjugal\" title=\"Saiba mais sobre Investigação Conjugal\">
                    <div class=\"item\">
                        <img src=\"img/galery/conjugal.jpg\" alt=\"\">
                        <div class=\"caption\">
                            <h2>";
        // line 142
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "investigacao", array(), "array"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "conjugal", array(), "array"), "html", null, true);
        echo "</span></h2>
                        </div>          
                    </div>
                </a>

                <a class=\"col-xs-12 col-md-3\" href=\"/";
        // line 147
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/investigacao-empresarial\" title=\"Saiba mais sobre Investigação Empresarial\">
                    <div class=\"item\">
                        <img src=\"img/galery/empresarial.jpg\" alt=\"\">
                        <div class=\"caption\">
                            <h2>";
        // line 151
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "investigacao", array(), "array"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "empresarial", array(), "array"), "html", null, true);
        echo "</span></h2>
                        </div>          
                    </div>
                </a>

                <a class=\"col-xs-12 col-md-3\" href=\"/";
        // line 156
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/investigacao-politica\" title=\"Saiba mais sobre Investigação Política\">
                    <div class=\"item\">
                        <img src=\"img/galery/politica.jpg\" alt=\"\">
                        <div class=\"caption\">
                            <h2>";
        // line 160
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "investigacao", array(), "array"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "politica", array(), "array"), "html", null, true);
        echo "</span></h2>
                        </div>      
                    </div>
                </a>

                <a class=\"col-xs-12 col-md-3\" href=\"/";
        // line 165
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/investigacao-especifica\" title=\"Saiba mais sobre Investigação Específica\">
                    <div class=\"item\">
                        <img src=\"img/galery/especifica.jpg\" alt=\"\">
                        <div class=\"caption\">
                            <h2>";
        // line 169
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "investigacao", array(), "array"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "especifica", array(), "array"), "html", null, true);
        echo "</span></h2>
                        </div>          
                    </div>
                </a>

                <a class=\"col-xs-12 col-md-3\" href=\"/";
        // line 174
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/localizacao-pessoal\" title=\"Saiba mais sobre Localização de Pessoas\">
                    <div class=\"item\">
                        <img src=\"img/galery/localizacao.jpg\" alt=\"\">
                        <div class=\"caption\">
                            <h2>";
        // line 178
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "localizacao", array(), "array"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "pessoas", array(), "array"), "html", null, true);
        echo "</span></h2>
                        </div>          
                    </div>
                </a>

                <a class=\"col-xs-12 col-md-3\" href=\"/";
        // line 183
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/localizacao-bens\" title=\"Saiba mais sobre Localização de Bens\">
                    <div class=\"item\">
                        <img src=\"img/galery/apreensao.jpg\" alt=\"\">
                        <div class=\"caption\">
                            <h2>";
        // line 187
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "localizacao", array(), "array"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "bens", array(), "array"), "html", null, true);
        echo "</span></h2>
                        </div>          
                    </div>
                </a>

                <a class=\"col-xs-12 col-md-3\" href=\"/";
        // line 192
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/monitoramento-virtual\" title=\"Saiba mais sobre Monitoramento Virtual\">
                    <div class=\"item\">
                        <img src=\"img/galery/monitoramentov.jpg\" alt=\"\">
                        <div class=\"caption\">
                            <h2>";
        // line 196
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "monitoramento", array(), "array"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "virtual", array(), "array"), "html", null, true);
        echo "</span></h2>
                        </div>      
                    </div>
                </a>

                <a class=\"col-xs-12 col-md-3\" href=\"/";
        // line 201
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/monitoramento-pessoal\" title=\"Saiba mais sobre Monitoramento Pessoal\">
                    <div class=\"item\">
                        <img src=\"img/galery/monitoramentop.jpg\" alt=\"\">
                        <div class=\"caption\">
                            <h2>";
        // line 205
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "monitoramento", array(), "array"), "html", null, true);
        echo " <span>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "pessoal", array(), "array"), "html", null, true);
        echo "</span></h2>
                        </div>          
                    </div>
                </a>

            </div>

            <div class=\"row section-row\">
                <div class=\"col-xs-12\">
                    <div class=\"container\">
                        <div class=\"row\">

                            <div class=\"col-xs-12 col-md-6\">
                                <ul class=\"list-unstyled\">
                                    <li><a href=\"/";
        // line 219
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/investigacao-juridica\" title=\"Saiba mais sobre Investigação Jurídica\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "servicos_a1", array(), "array"), "html", null, true);
        echo "</a></li>
                                    <li><a href=\"/";
        // line 220
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/flagrantes\" title=\"Saiba mais sobre Flagrantes\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "servicos_a2", array(), "array"), "html", null, true);
        echo "</a></li>
                                    <li><a href=\"/";
        // line 221
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/localizacao-animais\" title=\"Saiba mais sobre Localização de Animais\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "servicos_a3", array(), "array"), "html", null, true);
        echo "</a></li>
                                    <li><a href=\"/";
        // line 222
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/investigacao-prematrimonial\" title=\"Saiba mais sobre Investigação Conjugal pré-matrimonial\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "servicos_a4", array(), "array"), "html", null, true);
        echo "</a></li>
                                </ul>
                            </div>

                            <div class=\"clearfix visible-xs visible-sm\">
                                <br> <br>
                            </div>

                            <div class=\"col-xs-12 col-md-6\">
                                <p class=\"text-justify\" style=\"margin:0\">";
        // line 231
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "servicos_legal", array(), "array"), "html", null, true);
        echo "</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row section-row\">
                <div class=\"col-xs-12\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xs-12\">
                                <h2 class=\"text-right\"><span class=\"text-featured\">";
        // line 244
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "cobertura", array(), "array"), "html", null, true);
        echo "</span> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "de_servicos", array(), "array"), "html", null, true);
        echo "</h2>

                                <br>

                                <p class=\"text-justify\">";
        // line 248
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "cobertura_de_servicos", array(), "array"), "html", null, true);
        echo "</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row section-row\" id=\"contact\">
                <div class=\"col-xs-12\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-xs-12\">
                                <h2 class=\"text-left\"><span class=\"text-featured\">";
        // line 260
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "contate", array(), "array"), "html", null, true);
        echo "</span> ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "nossos_detetives", array(), "array"), "html", null, true);
        echo "</h2>

                                <br>

                                <p class=\"text-justify\">";
        // line 264
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "contate_nossos_detetives_p1", array(), "array"), "html", null, true);
        echo "</p>
                                <p class=\"text-justify\">";
        // line 265
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "contate_nossos_detetives_p2", array(), "array"), "html", null, true);
        echo "</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row contact-row\">
                <div class=\"col-xs-12\">
                    <div class=\"container\">
                        <div class=\"row\">
                            <a class=\"col-xs-6 col-sm-1\" href=\"https://www.facebook.com/pumadetetives.br\" rel=\"Facebook\" title=\"Facebook\">
                                <i class=\"fa fa-facebook\"></i>
                            </a>
                            <a class=\"col-xs-6 col-md-5\" href=\"mailto:pumadetetives@outlook.com\" rel=\"email\" title=\"Envie um email\">
                                <i class=\"fa fa-envelope\"></i> <span class=\"visible-md-inline visible-lg-inline\">pumadetetives@outlook.com</span>
                            </a>
                            <a class=\"col-xs-12 col-md-3\" href=\"tel:55999223814\" title=\"Ligar\">
                                <b><i class=\"fa fa-phone\"></i> <span>055 9 9922-3814</span></b>
                            </a>
                            <a class=\"col-xs-12 col-md-3\" href=\"tel:55996741018\" title=\"Ligar\">
                                <b><i class=\"fa fa-phone\"></i> <span>055 9 9674-1018</span></b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class=\"row\">
                <nav class=\"navbar navbar-default\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"/\" title=\"Puma Detetives\">
                                <img src=\"img/logo.png\" alt=\"Puma Detetives\"><h1>Puma Detetives</h1>
                            </a>
                        </div>
                        <div class=\"navbar-right visible-md-block visible-lg-block\">
                            <ul class=\"nav navbar-nav\">
                                <li><a href=\"/#about\" title=\"Sobre a Empresa\">Sobre</a></li>
                                <li><a href=\"#services\" title=\"Serviços Oferecidos\">Serviços</a></li>
                                <li><a href=\"#contact\" title=\"Contate um de nossos detetives\">Contato</a></li>
                                <li><a href=\"#top\" title=\"Voltar ao topo da página\">Voltar ao Topo</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

        </div>

        <script type=\"text/javascript\" src=\"//code.jquery.com/jquery-1.11.3.min.js\"></script>
        <script type=\"text/javascript\" src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js\"></script>
        <script type=\"text/javascript\" src=\"http://localhost/js/main-dev.js\"></script>

        <script type=\"text/javascript\">
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-58024066-1', 'auto');ga('send', 'pageview');
        </script>

    </body>

</html>";
    }

    // line 32
    public function block_head($context, array $blocks = array())
    {
        // line 33
        echo "            ";
        // line 34
        echo "        ";
    }

    // line 114
    public function block_content($context, array $blocks = array())
    {
        // line 115
        echo "                ";
        // line 116
        echo "            ";
    }

    public function getTemplateName()
    {
        return "template.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  489 => 116,  487 => 115,  484 => 114,  480 => 34,  478 => 33,  475 => 32,  406 => 265,  402 => 264,  393 => 260,  378 => 248,  369 => 244,  353 => 231,  339 => 222,  333 => 221,  327 => 220,  321 => 219,  302 => 205,  295 => 201,  285 => 196,  278 => 192,  268 => 187,  261 => 183,  251 => 178,  244 => 174,  234 => 169,  227 => 165,  217 => 160,  210 => 156,  200 => 151,  193 => 147,  183 => 142,  176 => 138,  163 => 128,  154 => 124,  145 => 117,  143 => 114,  67 => 41,  63 => 40,  56 => 35,  54 => 32,  21 => 1,);
    }
}
