<?php

/* index.twig */
class __TwigTemplate_18fea75008bac3562a948d03124502522d2cbc3f3f6caebb3ee37b69e6dd2da7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("template.twig", "index.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "
    <title>";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "title", array(), "array"), "html", null, true);
        echo "</title>
    <meta name=\"description\" content=\"Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.\">

    <meta property=\"og:title\" content=\"Puma Detetives\">
    <meta property=\"og:url\" content=\"http://www.pumadetetives.com.br\">
    <meta property=\"og:description\" content=\"Serviços de investigação particular com profissionalismo, ética e discrição total. Para escutas, fotos, filmagens, localizações contrate nossos detetives.\">

";
    }

    // line 14
    public function block_content($context, array $blocks = array())
    {
        // line 15
        echo "
    <div class=\"row carrousel\">

        <span class=\"progress\"></span>

        <div class=\"col-xs-12 current\" style=\"background-image:url(img/carrousel/dossier.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1 class=\"text-center text-adaptative\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "carrousel_inicial", array(), "array"), "html", null, true);
        echo "</h1>

                        <center>
                            <a href=\"#services\" class=\"btn btn-inverse\">";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "veja_nossos_servicos", array(), "array"), "html", null, true);
        echo "</a>
                            <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "contate_nos", array(), "array"), "html", null, true);
        echo "</a>
                        </center>

                    </div>
                </div>
            </div>

        </div>

        <div class=\"col-xs-12\" style=\"background-image:url(img/carrousel/virtual.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1>";
        // line 45
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "monitoramento_virtual", array(), "array"), "html", null, true);
        echo "</h1>
                        <h3>";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "monitoramento_virtual_descricao", array(), "array"), "html", null, true);
        echo "</h3>
                        <a href=\"/";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/monitoramento-virtual\" class=\"btn btn-inverse\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "saiba_mais", array(), "array"), "html", null, true);
        echo "</a>
                        <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "contate_nos", array(), "array"), "html", null, true);
        echo "</a>

                    </div>
                </div>
            </div>

        </div>

        <div class=\"col-xs-12\" style=\"background-image:url(img/carrousel/dossier.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1>";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "dossies_fisicos_juridicos", array(), "array"), "html", null, true);
        echo "</h1>
                        <h3>";
        // line 63
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "dossies_fisicos_juridicos_descricao", array(), "array"), "html", null, true);
        echo "</h3>
                        <a href=\"/";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/investigacao-juridica\" class=\"btn btn-inverse\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "saiba_mais", array(), "array"), "html", null, true);
        echo "</a>
                        <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "contate_nos", array(), "array"), "html", null, true);
        echo "</a>

                    </div>
                </div>
            </div>

        </div>

        <div class=\"col-xs-12\" style=\"background-image:url(img/carrousel/empresarial.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1>";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "investigacao_empresarial", array(), "array"), "html", null, true);
        echo "</h1>
                        <h3>";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "investigacao_empresarial_descricao", array(), "array"), "html", null, true);
        echo "</h3>
                        <a href=\"/";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/investigacao-empresarial\" class=\"btn btn-inverse\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "saiba_mais", array(), "array"), "html", null, true);
        echo "</a>
                        <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "contate_nos", array(), "array"), "html", null, true);
        echo "</a>

                    </div>
                </div>
            </div>

        </div>

        <div class=\"col-xs-12\" style=\"background-image:url(img/carrousel/juridico.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1>";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "investigacao_juridica", array(), "array"), "html", null, true);
        echo "</h1>
                        <h3>";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "investigacao_juridica_descricao", array(), "array"), "html", null, true);
        echo "</h3>
                        <a href=\"/";
        // line 98
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/investigacao-juridica\" class=\"btn btn-inverse\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "saiba_mais", array(), "array"), "html", null, true);
        echo "</a>
                        <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 99
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "contate_nos", array(), "array"), "html", null, true);
        echo "</a>

                    </div>
                </div>
            </div>

        </div>

        <div class=\"col-xs-12\" style=\"background-image:url(img/carrousel/infidelidade.jpg)\">

            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1>";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "infidelidade_conjugal", array(), "array"), "html", null, true);
        echo "</h1>
                        <h3>";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "infidelidade_conjugal_descricao", array(), "array"), "html", null, true);
        echo "</h3>
                        <a href=\"/";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "lang", array(), "array"), "html", null, true);
        echo "/investigacao-conjugal\" class=\"btn btn-inverse\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "saiba_mais", array(), "array"), "html", null, true);
        echo "</a>
                        <a href=\"#contact\" class=\"btn btn-inverse contact-modal-toggle\">";
        // line 116
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "contate_nos", array(), "array"), "html", null, true);
        echo "</a>

                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class=\"row section-row\" id=\"about\">
        <div class=\"col-xs-12\">
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xs-12\">

                        <h1 class=\"text-center\">
                            ";
        // line 133
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "trabalhos_investigativos_com", array(), "array"), "html", null, true);
        echo "
                            <span class=\"text-rotator\">
                                <span>";
        // line 135
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "etica", array(), "array"), "html", null, true);
        echo "</span>
                                <span>";
        // line 136
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "tecnologia", array(), "array"), "html", null, true);
        echo "</span>
                                <span>";
        // line 137
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "sigilo", array(), "array"), "html", null, true);
        echo "</span>
                                <span>";
        // line 138
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "discricao", array(), "array"), "html", null, true);
        echo "</span>
                            </span>
                        </h1>

                        <p class=\"text-justify\">";
        // line 142
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "agencia_descricao_p1", array(), "array"), "html", null, true);
        echo "</p>
                        <p class=\"text-justify\">";
        // line 143
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "agencia_descricao_p2", array(), "array"), "html", null, true);
        echo "</p>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class=\"row section-row\">
        <div class=\"col-xs-12\">
            <div class=\"container\">
                <div class=\"row\">

                    <div class=\"col-xs-12 col-md-4\">
                        <p class=\"text-justify\">";
        // line 157
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "agencia_descricao_confianca", array(), "array");
        echo "</p>
                    </div>

                    <div class=\"clearfix visible-xs-block\"></div>

                    <div class=\"col-xs-12 col-md-4\">
                        <p class=\"text-justify\">";
        // line 163
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "agencia_descricao_exito", array(), "array");
        echo "</p>
                    </div>

                    <div class=\"clearfix visible-xs-block\"></div>

                    <div class=\"col-xs-12 col-md-4\">
                        <p class=\"text-justify\">";
        // line 169
        echo $this->getAttribute((isset($context["lang"]) ? $context["lang"] : null), "agencia_descricao_sigilo", array(), "array");
        echo "</p>
                    </div>

                </div>
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  312 => 169,  303 => 163,  294 => 157,  277 => 143,  273 => 142,  266 => 138,  262 => 137,  258 => 136,  254 => 135,  249 => 133,  229 => 116,  223 => 115,  219 => 114,  215 => 113,  198 => 99,  192 => 98,  188 => 97,  184 => 96,  167 => 82,  161 => 81,  157 => 80,  153 => 79,  136 => 65,  130 => 64,  126 => 63,  122 => 62,  105 => 48,  99 => 47,  95 => 46,  91 => 45,  73 => 30,  69 => 29,  63 => 26,  50 => 15,  47 => 14,  35 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
