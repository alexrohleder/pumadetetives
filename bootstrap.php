<?php

$twig = new Slim\Views\Twig;
$twig->twigTemplateDirs = realpath(__DIR__."/templates");
$twig->parserOptions["cache"] = "{$twig->twigTemplateDirs}/cache";
$twig->parserOptions["auto_reload"] = true;

$app = new Slim\Slim(array(
    "view" => $twig
));

$app->notFound(function () use ($app) {
	$app->render("404.twig", array("lang" => translateTo("pt", "404")));
});

function translateTo($language, $page = 'index') {
	global $app;

	if (file_exists(__DIR__ . "/translations/$language/main.php")) {
		$lang = include "translations/$language/main.php";
		return array_merge($lang, include "translations/$language/$page.php");
	} else $app->notFound();
}
