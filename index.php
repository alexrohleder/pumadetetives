<?php

include './vendor/autoload.php';
include './bootstrap.php';

$app->get('/(:language)', function ($language = 'pt') use ($app) {
	if (is_file('./translations/'.$language.'/index.php')) {
    	   $app->render('index.twig', array('lang' => translateTo($language)));
	} else $app->notFound();
});

$app->get('/:language/:name', function ($language, $name) use ($app) {
	if (is_file('./translations/'.$language.'/'.$name.'.php')) {
		   $app->render('service.twig', array('lang' => translateTo($language, $name), 'uri' => $_SERVER['REQUEST_URI']));
	} else $app->notFound();
});

$app->run();
