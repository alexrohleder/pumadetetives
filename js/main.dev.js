
(function( window, document ) {
    
    'use strict';

    var methods=["assert","clear","count","debug","dir","dirxml","error","exception","group","groupCollapsed","groupEnd","info",
    "log","markTimeline","profile","profileEnd","table","time","timeEnd","timeStamp","trace","warn"],length=methods.length;
    for(window.console=window.console||{};length--;)window.console[methods[length]]||(window.console[methods[length]]=new Function);
    window.browsehappy={element:document.getElementById("browsehappy"),modernizr:document.body.parentNode.className,
    setMinimumBrowserRequirements:function(e){for(var o=0;o<e.length;o++)if(-1===window.browsehappy.modernizr.indexOf(e[o]))
    return window.browsehappy.element.style.display="block"},setMinimumIeCompatibility:function(e){
    return window.browsehappy.modernizr.indexOf("lt-ie"+e)>-1?window.browsehappy.element.style.display="block":void 0}};

    window.browsehappy.setMinimumBrowserRequirements([]);
    window.browsehappy.setMinimumIeCompatibility(9);
    
    var LandingPage = function (callback) {
        var ProgressController = function () {
            this.e = document.getElementsByClassName('tg').item(0);
            this.e.style.strokeDasharray = this.e.style.strokeDashoffset = this.e.getTotalLength();
        };
        
            ProgressController.prototype.draw = function (v) {
                this.e.style.strokeDashoffset = this.e.getTotalLength() * (1 - v);
            };
        
            ProgressController.prototype.setProgressFn = function (fn) {
                if (typeof fn === 'function') {
                    fn(this);
                }
            };
            
        var mpc = new ProgressController, c = 0;
            
        var events = {
            animEndName: (function () {
                switch (Modernizr.prefixed('animation')) {
                    case 'WebkitAnimation': return 'webkitAnimationEnd';
                    case 'OAnimation':      return 'oAnimationEnd';
                    case 'msAnimation':     return 'MSAnimationEnd';
                    case 'animation':       return 'animationend';
                }
            })(),
            onEndInitialAnimation: function () {
                if (Modernizr.cssanimations) {
                    // this.removeEventListener();
                }
                
                setTimeout(function () {
                    start();
                }, 500);
            },
            onEndHeaderAnimation: function (e) {
                if (Modernizr.cssanimations) {
                    if (e.target === document.body.querySelector('.landing')) {
                        this.removeEventListener(events.animEndName, events.onEndHeaderAnimation);
                    } else return;
                }

                var landing = document.getElementsByClassName('landing').item(0);
                    landing.parentNode.removeChild(landing);

                callback();
            }
        };
        
        var start = function () {
            mpc.setProgressFn(function (pc) {
                var interval = setInterval(function () {
                    c = Math.min(c + Math.random() * 0.1, 1);

                    pc.draw(c);

                    if (c === 1) {
                        document.body.classList.remove('loading');
                        document.body.classList.add('loaded');

                        clearInterval(interval);

                        if (Modernizr.cssanimations) {
                            var landing = document.getElementsByClassName('landing');

                            if (landing && landing.item(0)) {
                                landing.item(0)
                                       .addEventListener(events.animEndName, events.onEndHeaderAnimation);
                            }
                        } else events.onEndHeaderAnimation();
                    }
                }, 80);
            });
        };
        
        document.body.classList.add('loading');
        
        if (Modernizr.cssanimations) {
            document.body.addEventListener(events.animEndName, events.onEndInitialAnimation);
        } else events.onEndInitialAnimation();
    };
    
    var Carrousel = function () {
        var e = document.querySelectorAll('.carrousel > div'), c = e.length, n = 0;
        var p = document.querySelectorAll('.carrousel > .progress').item(0), i = 8000;
        var interval, width = 0;
        
        var progress = {
            start: function () {
                interval = setInterval(function () {
                    width++;
                    p.style.width = width + '%';
                }, i / 100);
            },
            reset: function () {
                width = 0;
                clearInterval(interval);
            }
        };
        
        var main = {
            next: function () {
                e.item(n).classList.remove('current');
                n = n < c - 1 ? n + 1 : 1;
                e.item(n).classList.add('current');
            },
            start: function () {
                if (Modernizr.cssanimations) {
                    progress.start();
                }

                setTimeout(function () {
                    progress.reset();
                    main.next();
                    main.start();
                }, i);
            }
        };
        
        main.start();
    };
    
    var TextRotator = function () {
        var e = document.querySelectorAll('.text-rotator > span'), c = e.length, i = 0;
        
        setInterval(function () {
            
            e.item(i === 0 ? 3 : i - 1).style.display = 'none';
            e.item(i).style.display = 'inline';
            
            if (i < c - 1) {
                i++;
            } else i = 0;
            
        }, 4000);
    };
    
    var VisibleContainerScroller = function () {
        if (Modernizr.touch) {
            return;
        }
        
        var getViewportHeight = function () {
            var client = document.documentElement.clientHeight,
                inner = window.innerHeight;

            if(client < inner)
                 return inner;
            else return client;
        };
        
        var getOffset = function (e) {
            var offsetTop = 0, offsetLeft = 0;
            
            do {
                if (! isNaN(e.offsetTop)) {
                    offsetTop += e.offsetTop;
                }

                if (! isNaN(e.offsetLeft)) {
                    offsetLeft += e.offsetLeft;
                }
            } while (e = e.offsetParent);

            return {
                top : offsetTop,
                left : offsetLeft
            };
        };
        
        var inViewport = function (e) {
            var eh = e.offsetHeight,
                scrolled = window.pageYOffset || document.documentElement.scrollTop,
                viewed = scrolled + getViewportHeight(),
                et = getOffset(e).top,
                eb = et + eh;

            return eb <= viewed && eb >= scrolled;
    };
        
        var sections = Array.prototype.slice.call(document.querySelectorAll('.section-row > div'));
        var scroll = false;
        var initScroll = function () {
            sections.forEach( function( e, i ) {
                if (inViewport(e)) {
                    e.classList.add('scroller-animate');
                }
            });
            
            scroll = false;
        }
        
        sections.forEach( function( e, i ) {
            if(! inViewport(e)) {
                e.classList.add('scroller-init');
            }
        });
        
        window.addEventListener('scroll', function () {
            if(! scroll) {
                
                scroll = true;
                
                setTimeout(function() {
                    initScroll();
                }, 60);
                
            }
        }, false);
        
        window.addEventListener('resize', function () {
            if (resizeTimeout) {
                clearTimeout(resizeTimeout);
            }
            
            var resizeTimeout = setTimeout(function () {
                initScroll();
            }, 200);
        }, false);
    };

    TextRotator();
    VisibleContainerScroller();
    
    LandingPage(function () {
        Carrousel();
    });
    
})( window, document );
